package emilianwilczek;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class GuessGame
{
    public static Scanner keyboard = new Scanner(System.in);

    public void startGame()
    {
        //Creating target number
        int targetNumber = (int)(Math.random() * 10);

        //Asking for number of players
        System.out.println("How many players are playing?");
        int numberOfPlayers = keyboard.nextInt();

        //Creating Players and making guesses
        List<Player> players = new ArrayList<Player>();
        for (int i = 0; i < numberOfPlayers; i++)
        {
            //Creating a player
            players.add(new Player());
            Player currentPlayer = players.get(i);
            currentPlayer.playerNumber = i;

            //Making a guess
            currentPlayer.guess();
            currentPlayer.guessedCorrect = currentPlayer.guessedNumber == targetNumber;

        }

        //Displaying results
        for (int j = 0; j <numberOfPlayers; j++)
        {
            Player currentPlayer = players.get(j);

            if (currentPlayer.guessedCorrect)
            {
                System.out.println("Player " + (currentPlayer.playerNumber+1) + " guessed the correct number!");
            }
            else
            {
                System.out.println("Player " + (currentPlayer.playerNumber+1) + " didn't guess the correct number...");
            }
        }
    }
}
