package emilianwilczek;

import java.util.Scanner;

public class Player
{
    int playerNumber;
    int guessedNumber;
    boolean guessedCorrect;

    public void guess()
    {
        System.out.println("Player " + (playerNumber+1) + ", please guess a number between 1 and 10");
        Scanner keyboard = new Scanner(System.in);
        guessedNumber = keyboard.nextInt();
        System.out.println("Player " + (playerNumber+1) + " guessed " + guessedNumber);
        System.out.println("------------------------------------------------");
    }
}
